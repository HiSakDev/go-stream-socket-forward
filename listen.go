package main

import (
	"errors"
	"fmt"
	"io"
	"net"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/v22/activation"
	"github.com/mdlayher/vsock"

	"gitlab.com/HiSakDev/go-stream-socket-forward/udp"
)

type MixIn struct {
	network       string
	path          string
	address       net.Addr
	cid           uint32
	port          uint32
	udpAcceptSize int
	udpQueueSize  int
	udpTTL        time.Duration
}

func NewMixIn(f string) (*MixIn, error) {
	m := &MixIn{}
	if f == "-" {
		m.network = "std"
	} else {
		var err error
		v := strings.Split(f, ":")
		switch len(v) {
		case 2:
			m.network = strings.ToLower(v[0])
			switch m.network {
			case "unix":
				if strings.HasPrefix(v[1], "@") {
					m.path = v[1]
				} else if v[1] != "" {
					m.path, err = filepath.Abs(v[1])
				}
			default:
				err = fmt.Errorf("unexpected network type: %s", m.network)
			}
		case 3:
			m.network = strings.ToLower(v[0])
			switch m.network {
			case "tcp":
				m.address, err = net.ResolveTCPAddr(m.network, strings.Join(v[1:], ":"))
			case "udp":
				m.address, err = net.ResolveUDPAddr(m.network, strings.Join(v[1:], ":"))
			case "vsock":
				if v[1] == "" {
					m.cid, err = vsock.ContextID()
				} else {
					var cid uint64
					cid, err = strconv.ParseUint(v[1], 10, 32)
					m.cid = uint32(cid)
				}
				if err == nil {
					var port uint64
					port, err = strconv.ParseUint(v[2], 10, 32)
					m.port = uint32(port)
				}
			default:
				err = fmt.Errorf("unexpected network type: %s", m.network)
			}
		default:
			err = errors.New("invalid address format")
		}
		if err != nil {
			return nil, err
		}
	}
	return m, nil
}

func (m *MixIn) SetUDP(acceptSize, queueSize int, ttl time.Duration) {
	m.udpAcceptSize = acceptSize
	m.udpQueueSize = queueSize
	m.udpTTL = ttl
}

func (m *MixIn) Listen() (net.Listener, error) {
	switch m.network {
	case "tcp":
		return net.Listen(m.network, m.address.String())
	case "udp":
		return udp.Listen(m.network, m.address.(*net.UDPAddr), m.udpAcceptSize, m.udpQueueSize, m.udpTTL)
	case "unix":
		return net.Listen(m.network, m.path)
	case "vsock":
		return vsock.ListenContextID(m.cid, m.port, nil)
	default:
		if listen, err := m.activationListen(); err != nil {
			return nil, err
		} else if listen != nil {
			return listen, nil
		}
		return &StdIn{}, nil
	}
}

func (m *MixIn) activationListen() (listen net.Listener, err error) {
	for _, f := range activation.Files(true) {
		listen, err = net.FileListener(f)
		if errors.Is(err, syscall.EPROTONOSUPPORT) {
			listen, err = vsock.FileListener(f)
		} else if err != nil {
			listen, err = udp.FileListener(f, m.udpAcceptSize, m.udpQueueSize, m.udpTTL)
		}
		break
	}
	return
}

type StdIn struct {
	std *StdConn
}

func (s *StdIn) Accept() (net.Conn, error) {
	if s.std != nil {
		s.std.cond.Wait()
		return nil, io.EOF
	}
	conn, err := NewStdConn()
	s.std = conn
	return conn, err
}

func (s *StdIn) Close() error {
	return nil
}

func (s *StdIn) Addr() net.Addr {
	return &StdAddr{}
}
