package udp

import (
	"io"
	"net"
	"time"
)

type Conn struct {
	in     chan []byte
	out    *net.UDPConn
	addr   *net.UDPAddr
	ttl    time.Duration
	timer  *time.Timer
	key    string
	closed bool
}

func newConn(conn *net.UDPConn, addr *net.UDPAddr, size int, ttl time.Duration) *Conn {
	return &Conn{
		in:    make(chan []byte, size),
		out:   conn,
		addr:  addr,
		ttl:   ttl,
		timer: time.NewTimer(ttl),
		key:   addr.String(),
	}
}

func (u *Conn) Read(b []byte) (int, error) {
	if u.closed {
		return 0, net.ErrClosed
	}
	select {
	case <-u.timer.C:
		u.closed = true
		return 0, io.EOF
	case v := <-u.in:
		u.timer.Stop()
		defer u.timer.Reset(u.ttl)
		return copy(b, v), nil
	}
}

func (u *Conn) Write(b []byte) (int, error) {
	if u.closed {
		return 0, net.ErrClosed
	}
	u.timer.Stop()
	defer u.timer.Reset(u.ttl)
	return u.out.WriteTo(b, u.addr)
}

func (u *Conn) Close() error {
	if u.closed {
		return nil
	}
	u.closed = true
	u.timer.Stop()
	u.timer.Reset(0)
	return nil
}

func (u *Conn) LocalAddr() net.Addr {
	return u.out.LocalAddr()
}

func (u *Conn) RemoteAddr() net.Addr {
	return u.addr
}

func (u *Conn) SetDeadline(t time.Time) error {
	u.timer.Stop()
	u.timer.Reset(t.Sub(time.Now()))
	return nil
}

func (u *Conn) SetReadDeadline(t time.Time) error {
	u.timer.Stop()
	u.timer.Reset(t.Sub(time.Now()))
	return nil
}

func (u *Conn) SetWriteDeadline(t time.Time) error {
	u.timer.Stop()
	u.timer.Reset(t.Sub(time.Now()))
	return nil
}
