package udp

import (
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"time"
)

const (
	BufferSize     = 65535
	DefaultTimeout = 30 * time.Second
)

type Listener struct {
	*net.UDPConn
	tracks []*Conn
	accept chan *Conn
	ttl    time.Duration
	size   int
	ctx    context.Context
	cancel context.CancelFunc
}

func NewListener(conn *net.UDPConn, acceptSize, queueSize int, ttl time.Duration) (net.Listener, error) {
	if ttl == 0 {
		ttl = DefaultTimeout
	}
	v := &Listener{
		UDPConn: conn,
		accept:  make(chan *Conn, acceptSize),
		ttl:     ttl,
		size:    queueSize,
	}
	v.ctx, v.cancel = context.WithCancel(context.Background())
	go v.run()
	return v, nil
}

func FileListener(f *os.File, acceptSize, queueSize int, ttl time.Duration) (net.Listener, error) {
	conn, err := net.FilePacketConn(f)
	if err != nil {
		return nil, err
	}
	if c, ok := conn.(*net.UDPConn); ok {
		return NewListener(c, acceptSize, queueSize, ttl)
	}
	return nil, fmt.Errorf("invalid conn type: %T", conn)
}

func Listen(network string, addr *net.UDPAddr, acceptSize, queueSize int, ttl time.Duration) (net.Listener, error) {
	conn, err := net.ListenUDP(network, addr)
	if err != nil {
		return nil, err
	}
	return NewListener(conn, acceptSize, queueSize, ttl)
}

func (u *Listener) pruneTrack() {
	tracks := make([]*Conn, 0)
	for _, track := range u.tracks {
		if !track.closed {
			tracks = append(tracks, track)
		}
	}
	u.tracks = tracks
}

func (u *Listener) getTrack(key string) *Conn {
	for _, track := range u.tracks {
		if !track.closed && track.key == key {
			return track
		}
	}
	return nil
}

func (u *Listener) run() {
	for {
		buf := make([]byte, BufferSize)
		n, addr, err := u.UDPConn.ReadFromUDP(buf)
		if err != nil {
			break
		}
		u.pruneTrack()
		track := u.getTrack(addr.String())
		if track == nil {
			track = newConn(u.UDPConn, addr, u.size, u.ttl)
			u.tracks = append(u.tracks, track)
			u.accept <- track
		}
		track.in <- buf[:n]
	}
}

func (u *Listener) Accept() (net.Conn, error) {
	select {
	case <-u.ctx.Done():
		return nil, net.ErrClosed
	case v, ok := <-u.accept:
		if !ok {
			return nil, io.ErrUnexpectedEOF
		}
		return v, nil
	}
}

func (u *Listener) Addr() net.Addr {
	return u.UDPConn.LocalAddr()
}

func (u *Listener) Close() error {
	defer u.cancel()
	return u.UDPConn.Close()
}
