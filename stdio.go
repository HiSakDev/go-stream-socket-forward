package main

import (
	"io"
	"net"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/muesli/cancelreader"
)

type StdConn struct {
	cond   *sync.Cond
	active atomic.Bool
	in     cancelreader.CancelReader
	out    io.Writer
}

func NewStdConn() (*StdConn, error) {
	r, err := cancelreader.NewReader(os.Stdin)
	if err != nil {
		return nil, err
	}
	conn := &StdConn{in: r, out: os.Stdout}
	conn.cond = sync.NewCond(&sync.Mutex{})
	conn.cond.L.Lock()
	conn.active.Store(true)
	return conn, nil
}

func (s *StdConn) Read(b []byte) (n int, err error) {
	return s.in.Read(b)
}

func (s *StdConn) Write(b []byte) (n int, err error) {
	return s.out.Write(b)
}

func (s *StdConn) Close() error {
	defer s.cond.Signal()
	defer s.active.Store(false)
	s.in.Cancel()
	return nil
}

func (s *StdConn) LocalAddr() net.Addr {
	return &StdAddr{}
}

func (s *StdConn) RemoteAddr() net.Addr {
	return &StdAddr{}
}

func (s *StdConn) SetDeadline(t time.Time) error {
	return nil
}

func (s *StdConn) SetReadDeadline(t time.Time) error {
	return nil
}

func (s *StdConn) SetWriteDeadline(t time.Time) error {
	return nil
}

type StdAddr struct {
}

func (s StdAddr) Network() string {
	return "std"
}

func (s StdAddr) String() string {
	return "stdio"
}
