package main

import (
	"errors"
	"fmt"
	"net"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/mdlayher/vsock"
)

type MixOut struct {
	network string
	path    string
	address net.Addr
	cid     uint32
	port    uint32

	std *StdConn
}

func NewMixOut(f string) (*MixOut, error) {
	m := &MixOut{}
	if f == "-" {
		m.network = "std"
	} else {
		var err error
		v := strings.Split(f, ":")
		switch len(v) {
		case 2:
			m.network = strings.ToLower(v[0])
			switch m.network {
			case "unix":
				if strings.HasPrefix(v[1], "@") {
					m.path = v[1]
				} else if v[1] != "" {
					m.path, err = filepath.Abs(v[1])
				}
			default:
				err = fmt.Errorf("unexpected network type: %s", m.network)
			}
		case 3:
			m.network = strings.ToLower(v[0])
			switch m.network {
			case "tcp":
				m.address, err = net.ResolveTCPAddr(m.network, strings.Join(v[1:], ":"))
			case "udp":
				m.address, err = net.ResolveUDPAddr(m.network, strings.Join(v[1:], ":"))
			case "vsock":
				if v[1] == "" {
					m.cid, err = vsock.ContextID()
				} else {
					var cid uint64
					cid, err = strconv.ParseUint(v[1], 10, 32)
					m.cid = uint32(cid)
				}
				if err == nil {
					var port uint64
					port, err = strconv.ParseUint(v[2], 10, 32)
					m.port = uint32(port)
				}
			default:
				err = fmt.Errorf("unexpected network type: %s", m.network)
			}
		default:
			err = errors.New("invalid address format")
		}
		if err != nil {
			return nil, err
		}
	}
	return m, nil
}

func (m *MixOut) Dial() (net.Conn, error) {
	switch m.network {
	case "tcp":
		return net.Dial(m.network, m.address.String())
	case "udp":
		return net.Dial(m.network, m.address.String())
	case "unix":
		return net.Dial(m.network, m.path)
	case "vsock":
		return vsock.Dial(m.cid, m.port, nil)
	default:
		if m.std != nil && m.std.active.Load() {
			return nil, errors.New("already connected")
		}
		conn, err := NewStdConn()
		m.std = conn
		return m.std, err
	}
}
