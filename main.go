package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"sync/atomic"
	"time"

	"github.com/pires/go-proxyproto"

	"gitlab.com/HiSakDev/go-stream-socket-forward/udp"
)

var (
	version              string
	commit               string
	date                 string
	listenFormat         string
	connectFormat        string
	listenProxyProtocol  bool
	connectProxyProtocol uint
	udpAcceptSize        int
	udpQueueSize         int
	udpTTL               time.Duration
	verbose              bool
	timestamp            bool
	lineNumber           bool
)

func main() {
	flag.StringVar(&listenFormat, "listen", "-", "listen address")
	flag.StringVar(&connectFormat, "connect", "-", "connect address")
	flag.BoolVar(&listenProxyProtocol, "listen-proxy", false, "enable listen proxy protocol")
	flag.UintVar(&connectProxyProtocol, "connect-proxy", 0, "connect proxy protocol version")
	flag.IntVar(&udpAcceptSize, "udp-accept-size", 256, "udp accept buffer size")
	flag.IntVar(&udpQueueSize, "udp-queue-size", 64, "udp queue buffer size")
	flag.DurationVar(&udpTTL, "udp-ttl", udp.DefaultTimeout, "udp conntrack timeout")
	flag.BoolVar(&verbose, "verbose", false, "enable logging")
	flag.BoolVar(&timestamp, "timestamp", false, "enable logging timestamp")
	flag.BoolVar(&lineNumber, "line-number", false, "enable logging line number")
	isVersion := flag.Bool("version", false, "show version")
	flag.Parse()
	if *isVersion {
		fmt.Printf("version: %s\n", version)
		fmt.Printf("commit: %s\n", commit)
		fmt.Printf("date: %s\n", date)
		os.Exit(0)
	}
	var logFlags int
	if timestamp {
		logFlags |= log.LstdFlags
	}
	if lineNumber {
		logFlags |= log.Lshortfile
	}
	log.SetFlags(logFlags)
	if !verbose {
		log.SetOutput(io.Discard)
	}
	if listenFormat == "-" && connectFormat == "-" {
		log.Fatal("Error because both listen and dial have same stdio")
	}
	in, err := NewMixIn(listenFormat)
	if err != nil {
		log.Fatal(err)
	}
	in.SetUDP(udpAcceptSize, udpQueueSize, udpTTL)
	out, err := NewMixOut(connectFormat)
	if err != nil {
		log.Fatal(err)
	}
	serve(in, out)
}

func serve(in *MixIn, out *MixOut) {
	c := &atomic.Uint64{}
	listen, err := in.Listen()
	if err != nil {
		log.Fatal(err)
	} else {
		if listenProxyProtocol {
			listen = &proxyproto.Listener{
				Listener:          listen,
				ReadHeaderTimeout: 10 * time.Second,
			}
		}
		defer listen.Close()
		log.Printf("listening on %s", listen.Addr())
		for {
			if conn, err := listen.Accept(); err != nil {
				if !errors.Is(err, io.EOF) {
					log.Printf("accept failed: %v", err)
				}
				return
			} else {
				go forward(conn, out, c.Add(1))
			}
		}
	}
}

func forward(conn net.Conn, out *MixOut, count uint64) {
	var addrLogSuffix string
	switch v := conn.(type) {
	case *proxyproto.Conn:
		_, ok := v.Raw().(*net.UnixConn)
		var rawLogSuffix string
		if v.ProxyHeader() != nil {
			if ok {
				rawLogSuffix = fmt.Sprintf(" remote %s", v.RemoteAddr())
			} else {
				rawLogSuffix = fmt.Sprintf(" raw %s", v.Raw().RemoteAddr())
			}
		} else if ok {
			rawLogSuffix = fmt.Sprintf(" count %d", count)
		}
		if ok {
			addrLogSuffix = fmt.Sprintf(" from %s%s", v.Raw().LocalAddr(), rawLogSuffix)
		} else {
			addrLogSuffix = fmt.Sprintf(" from %s%s", v.RemoteAddr(), rawLogSuffix)
		}
	case *net.UnixConn:
		addrLogSuffix = fmt.Sprintf(" from %s count %d", conn.LocalAddr(), count)
	default:
		addrLogSuffix = fmt.Sprintf(" from %s", conn.RemoteAddr())
	}
	defer conn.Close()
	toConn, err := out.Dial()
	if err != nil {
		log.Printf("connection failed %v%s", err, addrLogSuffix)
	} else {
		defer toConn.Close()
		if out.network == "unix" {
			log.Printf("connection established %s%s", toConn.RemoteAddr(), addrLogSuffix)
		} else {
			log.Printf("connection established %s by destination %s%s", toConn.LocalAddr(), toConn.RemoteAddr(), addrLogSuffix)
		}

		if connectProxyProtocol > 0 {
			transport := proxyproto.TCPv4
			dst := toConn.RemoteAddr()
			if out.network != "tcp" && out.network != "udp" {
				dst = conn.LocalAddr()
			}
			src := conn.RemoteAddr()
			switch v := src.(type) {
			case *net.TCPAddr:
				if v.IP.To4() == nil {
					transport = proxyproto.TCPv6
				}
			case *net.UDPAddr:
				if v.IP.To4() == nil {
					transport = proxyproto.UDPv6
				} else {
					transport = proxyproto.UDPv4
				}
			case *net.UnixAddr:
				transport = proxyproto.UnixStream
				src = conn.LocalAddr()
			}
			header := &proxyproto.Header{
				Version:           byte(connectProxyProtocol),
				Command:           proxyproto.PROXY,
				TransportProtocol: transport,
				SourceAddr:        src,
				DestinationAddr:   dst,
			}
			if _, err = header.WriteTo(toConn); err != nil {
				log.Printf("proxy protocol failed: %v: %+v", err, header)
				return
			}
		}
		begin := time.Now()
		go func() {
			defer conn.Close()
			n, _ := io.Copy(conn, toConn)
			log.Printf("receiving data size is %d byte %s elapsed%s", n, time.Now().Sub(begin), addrLogSuffix)
		}()
		n, _ := io.Copy(toConn, conn)
		log.Printf("sending data size is %d byte %s elapsed%s", n, time.Now().Sub(begin), addrLogSuffix)
	}
}
