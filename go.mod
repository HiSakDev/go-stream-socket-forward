module gitlab.com/HiSakDev/go-stream-socket-forward

go 1.20

require (
	github.com/coreos/go-systemd/v22 v22.5.0
	github.com/mdlayher/vsock v1.2.1
	github.com/muesli/cancelreader v0.2.2
	github.com/pires/go-proxyproto v0.7.0
)

require (
	github.com/mdlayher/socket v0.4.1 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
)
